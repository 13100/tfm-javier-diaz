from cv2 import findContours as findContours
from cv2 import boundingRect as boundingRect
from cv2 import rectangle as rectangle
from cv2 import fitEllipse as fitEllipse
from cv2 import ellipse as Ellipse
from cv2 import fitLine as fitLine
from cv2 import DIST_L2 as DIST_L2
from cv2 import line as Line
from cv2 import minEnclosingCircle as minEnclosingCircle
from cv2 import circle as Circle
from numpy import pi as PI
from numpy import arccos as acos

from Distance import distance as distance
range_cnt = 120


def shapes(blurred, photo_full):
    im2, contours, hierarchy = findContours(photo_full, 3, 1)
    rad = 0
    deg = 0
    deg_360 = 0
    list_ellipse = []
    list_rect =[]
    list_line = []
    list_circle=[]
    for v in range(len(contours)):
        cnt = contours[v]
        if (len(cnt) > range_cnt):

            x_r, y_r, w_r, h_r = boundingRect(cnt)
            rect_aux = [x_r, y_r, w_r, h_r]
            list_rect.append(rect_aux)
            rectangle(photo_full, (x_r, y_r), (x_r + w_r, y_r + h_r), (255, 255, 255), 1)

            ellipse = fitEllipse(cnt)
            list_ellipse.append(ellipse)
            Ellipse(photo_full, ellipse, (0, 0, 0), 3)
            Ellipse(photo_full, ellipse, (255, 255, 255), 1)

            rows, cols = photo_full.shape[:2]
            [vx_l, vy_l, x_l, y_l] = fitLine(cnt, DIST_L2, 0, 0.01, 0.01)
            lefty = int((-x_l * vy_l / vx_l) + y_l)
            righty = int(((cols - x_l) * vy_l / vx_l) + y_l)
            line_aux = [lefty, righty]
            list_line.append(line_aux)
            Line(photo_full, (cols - 1, righty), (0, lefty), (0, 0, 0), 2)
            Line(photo_full, (cols - 1, righty), (0, lefty), (255, 255, 255), 1)

            hip = distance([cols - 1, righty], [0, lefty])
            rad = acos(127 / hip)
            deg = rad * 180 / PI
            deg_360 = deg
            if (lefty - righty) < 0:
                rad = -rad
                deg = -deg
                deg_360 = 360 + deg
            (x_c, y_c), radius = minEnclosingCircle(cnt)
            center = (int(x_c), int(y_c))
            radius = int(radius)
            cir_aux = [center, radius]
            list_circle.append(cir_aux)
            #print(list_circle, list_line, list_ellipse, list_rect)
            Circle(photo_full, center, radius, (255, 255, 255), 1)
    return [rad, deg, deg_360, list_ellipse, list_rect, list_circle, list_line]

