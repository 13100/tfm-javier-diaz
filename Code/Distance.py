from numpy import sqrt as sq

def distance(a, b):
    x = a[0] - b[0]
    y = a[1] - b[1]
    x2 = x * x
    y2 = y * y
    dis = sq(x2 + y2)
    return dis