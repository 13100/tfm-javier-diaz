from cv2 import imread as imread
from cv2 import COLOR_BGR2GRAY as COLOR_BGR2GRAY
from cv2 import cvtColor as cvtColor
from cv2 import GaussianBlur as GaussianBlur
from cv2 import threshold as threshold
from numpy import zeros as z
import cv2 as cv

"""def inicializar_video(path_in, num0, num1, num2, pos): #version sin zmq
    path = path_in
    num_0_str = str(num0)
    num_1_str = str(num1)
    num_2_str = str(num2)
    num = num_0_str + num_1_str + num_2_str
    path = path.replace("001", num)
    img = imread(path)
    photo_blank = z([128, 128])
    crop_img = img[pos[0]:(pos[0] + 128), pos[1]:(pos[1] + 128)]
    gray = cvtColor(crop_img, COLOR_BGR2GRAY)
    blurred = GaussianBlur(gray, (5, 5), 0)
    ret, thresh1 = threshold(blurred, 205, 255, 0)
    return [gray, blurred, thresh1, photo_blank, num]"""

def inicializar_zmq(img, pos):
    photo_blank = z([128, 128])
    #crop_img = img[pos[0]:(pos[0] + 128), pos[1]:(pos[1] + 128)]
    crop_img = img
    gray = cvtColor(crop_img, COLOR_BGR2GRAY)
    blurred = GaussianBlur(gray, (5, 5), 0)
    ret, thresh1 = threshold(blurred, 205, 255, 0)

    return [gray, blurred, thresh1]