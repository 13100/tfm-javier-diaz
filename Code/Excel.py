from numpy import zeros as z
from numpy import pi as PI


def writeExcel(dif_area, area_t, perimeter, area_good, rango, num, worksheet, workbook):
    dif_area_porcentual = z([rango], dtype=object)
    circularity = z([rango], dtype=object)
    for d in range(rango):
        dif_area_porcentual[d] = dif_area[d] / area_t[d] * 100
        circularity[d] = 4 * PI * area_good[d] / (perimeter[d] * perimeter[d])
        col = d
        row = int(num) + 1
        worksheet.write_number(row, col, area_good[d])
        col = d + rango + 1
        worksheet.write_number(row, col, dif_area[d])
        col = d + 2 * rango + 2
        worksheet.write_number(row, col, dif_area_porcentual[d])
    workbook.close()