import cv2 as cv
from numpy import zeros as z

from Shapes import shapes as shapes

def rellenar(blurred, salto, perimeter, rad, deg, deg_360, rango, start):
    photo_full = z([rango], dtype=object)
    photo_full_solo = z([rango], dtype=object)
    area_good = z([rango], dtype=object)
    area_bad = z([rango], dtype=object)
    list_ellipse = z([rango], dtype=object)
    list_rect = z([rango], dtype=object)
    list_circle = z([rango], dtype=object)
    list_line = z([rango], dtype=object)
    list_total = []
    area_blank = 0
    perimeter_blank = 0
    for i in range(0, rango):
        area_blank = 0
        photo_full[i] = cv.inRange(blurred, start + salto * i + 1, 255)
        photo_full_solo[i] = cv.inRange(blurred, start + salto * i + 1, 255)
        # im2, contours, hierarchy = cv.findContours(photo_full[i], 1, 2)
        im2, contours, hierarchy = cv.findContours(photo_full[i], 3, 1)
        if(len(contours)>0):
            cnt = contours[0]
            area_good[i] = cv.contourArea(cnt)
            area_bad[i] = cv.contourArea(cnt)

            for k in range(len(contours)):  #Calcula el area y perímetro de todos los contornos
                area_blank += cv.contourArea(contours[k])
                perimeter_blank += cv.arcLength(contours[k], True)
            # cv.fillPoly(photo_blank, pts=[contours[k]], color=[50 + 50 * k, 50 + 50 * k, 50 + 50 * k])

        ##############################################################
        rad[i], deg[i], deg_360[i], list_ellipse, list_rect, list_circle, list_line = shapes(blurred, photo_full[i])  #Calcula el eje mayor
        ##############################################################

        area_good[i] = area_blank
        perimeter[i] = perimeter_blank
        lists = [list_ellipse, list_rect, list_circle, list_line]
        list_total.append(lists)
    return [area_blank, photo_full, area_good, photo_full_solo, rad, deg, deg_360, list_total]
