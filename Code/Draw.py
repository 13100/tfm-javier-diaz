from cv2 import imshow as imshow
from cv2 import moveWindow as moveWindow
from cv2 import imwrite as save

def draw(full, contour, i, rango, full_solo):
    fila = 50
    imshow(str(i), full_solo)
    moveWindow(str(i), 100 + i * 150, fila)
    fila += 200

    imshow(str(rango * 2 + i), full)
    moveWindow(str(rango * 2 + i), 100 + i * 150, fila)
    fila += 200

    imshow(str(rango + i), contour)
    moveWindow(str(rango + i), 100 + i * 150, fila)
    fila += 200
    save('a'+str(i)+'.jpg', full)