
from zmq import Context as Context
from zmq import REP as REP
from time import sleep as sleep
from numpy import zeros as z
from cv2 import imread as imread

context = Context()
socket = context.socket(REP)
socket.bind("tcp://*:5555")
cont = "inicio"
#m = np.zeros([3,2])
m = z([100,100], dtype=object)

tipo_img = "muy fuerte"

if(tipo_img == "fuerte"):
    cont_path = 1
if (tipo_img == "normal"):
    cont_path = 2
if (tipo_img == "muy fuerte"):
    cont_path = 3

path_full = r'Temple_muy_fuerte\001.jpg'
if(cont_path==1):
    path_in = r'temple_fuerte\XXX.jpg'
if (cont_path==2):
    path_in = r'temple_normal\XXX.jpg'
if(cont_path==3):
    path_in = r'temple_muy_fuerte\XXX.jpg'
num = 1
while True:
    #  Wait for next request from client
    print("Waiting for message")
    message = socket.recv_string()

    #msg = message.decode("utf-8")
    #print("Received request: %s" % message)
    if(message =="getImg"):
        if(cont_path==3 and num==167):
            socket.send_pyobj("end")
            print("Send end")
        elif(cont_path == 2 and num ==70):
            socket.send_pyobj("end")
            print("Send end")
        elif (cont_path == 1 and num == 99):
            socket.send_pyobj("end")
            print("Send end")
        else:
            num_str = str(num)
            long = len(num_str)
            if(cont_path == 3):
                if (long == 1):
                    num_str = "00" + num_str
                elif (long == 2):
                    num_str = "0" + num_str
            else:
                if (long == 1):
                    num_str = "0" + num_str
            img_full = imread(path_full.replace("001", num_str))
            img = imread(path_in.replace("XXX", num_str))

            socket.send_pyobj(img)
            print("Img "+num_str+" sent")
            num += 1
    elif(message == "start"):
        num = 1
        socket.send_pyobj(0)
        print("Cadena reseteada")

    sleep(1/1000)