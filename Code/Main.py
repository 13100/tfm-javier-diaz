from numpy import zeros as z
from cv2 import waitKey as waitKey
from cv2 import imshow as imshow
from time import time as time
from xlsxwriter import Workbook as Workbook

from Excel import writeExcel as writeExcel
from Inicializar import inicializar_zmq as inicializar_zmq
from Areas import areas_zmq as areas_zmq
from zmq import Context as Context
from zmq import REQ as REQ

from cv2 import imshow as imshow

pos = [73, 185]
num_0 = 0
num_1 = 0
num_2 = 1
rango = 7
start = 150
#print("v para vídeo")
#bucle = input()
show = True
bucle = "z"
bucle_video = False
if(bucle == "v"):
    bucle_video = True
bucle_zmq = not bucle_video
flag_reset = False

area_t = z([rango], dtype=object)
area_t_1 = z([rango], dtype=object)
dif_area = z([rango], dtype=object)
circularity = z([rango], dtype=object)
perimeter = z([rango], dtype=object)
rad = z([rango], dtype=object)
deg = z([rango], dtype=object)
deg_360 = z([rango], dtype=object)
dif_area_porcentual = z([rango], dtype=object)

workbook = Workbook('Area.xlsx')
worksheet = workbook.add_worksheet()
count = 0
if(bucle_zmq):
    context = Context()  # 0.004987-0.002036
    socket = context.socket(REQ)
    socket.connect("tcp://10.2.41.171:5555")  # 10.2.41.171//localhost


"""while (bucle_video): #version sin zmq
    area_t_1 = area_t
    gray, blurred, thresh1, photo_blank, num = inicializar_video(path_in=r'Temple_muy_fuerte\001.jpg', num0=num_0, num1=num_1,
                                                           num2=num_2, pos=pos)

    area, area_good, area_blank, rad, deg, deg_360 = areas(start, rango, blurred, gray, radio_max, radio_min, perimeter,
                                                           rad, deg, deg_360, photo_blank)
    area_t = area_good
    dif_area = area_t - area_t_1
    # writeExcel(dif_area, area_t, perimeter, area_good, rango, num, worksheet, workbook)

    # print(circularity)

    # print(deg_360)
    waitKey(10)

    print(num)
    num_2 += 1
    if (num_2 == 10):
        num_2 = 0
        num_1 += 1
        if (num_1 == 10):
            num_1 = 0
            num_0 += 1
    if (num == "166"):
        bucle = False"""
########################################################################
while(bucle_zmq):
    t_0 = time()
    if(flag_reset):
        msg = "getImg"
    else:
        msg = "start"
    socket.send_string(msg)
    img = socket.recv_pyobj()
    if(type(img) == type(1)):
        flag_reset = True
    elif(type(img) == type("end")):
        flag_reset = False
        break
    else:
        gray, blurred, thresh1 = inicializar_zmq(img, pos) #devuelve la imagen tratada para ser examinada
        area_dif, area_good, rad, deg, deg_360, list_ellipse, list_rect, list_circle, list_line = areas_zmq(start,
                                                                                                            rango,
                                                                                                            blurred,
                                                                                                            perimeter,
                                                                                                            rad, deg,
                                                                                                            deg_360,
                                                                                                            show)
        # Examina las imágenes y saca los parámetros de interés

        """Datos importantes
        area_dif: Vector de dimensión rango que tiene la diferencia de area de cada rango de temperatura
        area_good: Vector de dimensión rango que tiene el area de cada rango de temperatura
        rad, deg, deg_360: la inclinación del eje mayor del area de cada rango de temperatura
        list_ellipse/list_rect/list_circle/list_line: Vector de dimension rango que tiene las elipses, rectángulos
                                                    círculos y líneas de cada rango de temperatura"""

        #print(area_dif, area_good, list_ellipse, list_rect, list_line, list_circle, deg_360)

        """aux_str_dif = ""
        for i in range(7):
            var = area_good
            if (i == 6):
                aux_str_dif = aux_str_dif + str(round(var[i], 2))
            else:
                aux_str_dif = aux_str_dif + str(round(var[i], 2)) + ","
        # print(aux_str_dif)

        aux_str_good = ""
        for i in range(7):
            var = area_good
            if(i==6):
                aux_str_dif = aux_str_dif + str(round(var[i], 2))
            else:
                aux_str_dif = aux_str_dif + str(round(var[i], 2)) + ","
        #print(aux_str_good)

        aux_str_deg = ""
        for i in range(7):
            var = deg_360
            if (i == 6):
                aux_str_deg = aux_str_deg + str(round(var[i], 2))
            else:
                aux_str_deg = aux_str_deg + str(round(var[i], 2)) + ","
        #print(aux_str_deg)



        aux_str_el = ""
        if(list_ellipse[0]==[]):
            #print("0, 0, 0, 0, 0")
            aux_str_el = ""
        else:
            #var = list_ellipse[0][0]
            a1 =list_ellipse[0][0][0][0]
            a2 =list_ellipse[0][0][0][1]
            a3 = list_ellipse[0][0][1][0]
            a4 = list_ellipse[0][0][1][1]
            a5 =list_ellipse[0][0][2]
            aux_str_el = str(round(a1, 2)) + ", "+ str(round(a2, 2)) + ", "+ str(round(a3, 2)) + ", "+ str(round(a4, 2)) + ", "+ str(round(a5, 2))
            #print(aux_str_el) #elipse

        aux_str_rec = ""
        if (list_rect[0] == []):
            #print("0, 0, 0, 0")
            aux_str_rec
        else:
            a1 = list_rect[0][0][0]
            a2 = list_rect[0][0][1]
            a3 = list_rect[0][0][2]
            a4 = list_rect[0][0][3]

            aux_str_rec = str(round(a1, 2)) + ", " + str(round(a2, 2)) + ", " + str(round(a3, 2)) + ", " + str(
                round(a4, 2))
            #print(aux_str_rec)  #rectangulo

        aux_str_lin = ""
        if (list_line[0] == []):
            #print("0, 0")
            aux_str_lin = ""
        else:
            a1 = list_rect[0][0][0]
            a2 = list_rect[0][0][1]

            aux_str_lin = str(round(a1, 2)) + ", " + str(round(a2, 2))
            #print(aux_str_lin)  #linea

        aux_str_cir = ""
        if (list_circle[0] == []):
            #print("0, 0, 0")
            aux_str_cir = ""
        else:
            a1 = list_circle[0][0][0][0]
            a2 = list_circle[0][0][0][1]
            a3 = list_circle[0][0][1]

            aux_str_cir = str(round(a1, 2)) + ", " + str(round(a2, 2)) + ", " + str(round(a3, 2))
            #print(aux_str_cir)  #circulo
            """

        if(show):
            waitKey(0)
            #print(count)
            #count += 1

