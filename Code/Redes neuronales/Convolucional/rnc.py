from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
import matplotlib.pyplot as plt
###Inicializar CNN
times = 10
for q in range(times):

    classifier = Sequential()

###Capa de convolución

    classifier.add(Conv2D(filters=32, kernel_size=(3, 3), input_shape=(128, 128, 3),
                      activation="relu"))
# filters = 32/64, kernel_size depende de lo grande que sea la foto, input_shape la de la foto 3 para
# color 1 para byn

###Capa MaxPooling

    classifier.add(MaxPooling2D(pool_size=(2, 2)))

###############################################################################
    classifier.add(Conv2D(filters=32, kernel_size=(3, 3),
                      activation="relu"))

    classifier.add(MaxPooling2D(pool_size=(2, 2)))
###############################################################################


###############################################################################
    classifier.add(Conv2D(filters=32, kernel_size=(3, 3),
                      activation="relu"))

    classifier.add(MaxPooling2D(pool_size=(2, 2)))
################################################################################

# cuanta más grande la matriz, más reducción, menos calidad y más velocidad

###Flatering

    classifier.add(Flatten())

###Full connection

    classifier.add(Dense(units=128, activation="relu"))

    classifier.add(Dense(units=128, activation="relu"))

    classifier.add(Dense(units=128, activation="relu"))

    classifier.add(Dense(units=1, activation="sigmoid"))

###Compilar la red neuronal de convolución

    classifier.compile(optimizer="adam", loss="binary_crossentropy", metrics=["accuracy"])

###Entrenar
    from keras.preprocessing.image import ImageDataGenerator

    train_datagen = ImageDataGenerator(
    rescale=1. / 255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True)

    test_datagen = ImageDataGenerator(rescale=1. / 255)

    training_dataset = train_datagen.flow_from_directory('dataset/training_set',
                                                     target_size=(128, 128),
                                                     batch_size=32,
                                                     class_mode='binary')

    testing_dataset = test_datagen.flow_from_directory('dataset/test_set',
                                                   target_size=(128, 128),
                                                   batch_size=32,
                                                   class_mode='binary')
    steps = 8
    epoch = 30
    val_step = 10

    """steps = 80
    epoch = 100
    val_step = 20"""

    history = classifier.fit_generator(training_dataset,
                         steps_per_epoch=steps,
                         epochs=epoch,
                         validation_data=testing_dataset,
                         validation_steps=val_step)

    """history = classifier.fit_generator(training_dataset,
                         steps_per_epoch=8000,
                         epochs=25,
                         validation_data=testing_dataset,
                         validation_steps=2000)"""


#retocar steps

    plt.plot(history.history['accuracy'],'r')
    plt.plot(history.history['val_accuracy'],'b')
    plt.title('model accuracy '+str(steps)+"-"+str(epoch)+"-"+str(val_step))
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
plt.show()

plt.savefig('Test_'+str(steps)+"_"+str(epoch)+"_"+str(val_step)+'.png')
plt.show()
from keras.models import model_from_json
model_json = classifier.to_json()
with open("model_conv.json", "w") as json_file:
    json_file.write(model_json)
classifier.save_weights("model_conv.h5")
print("Saved model to disk")
