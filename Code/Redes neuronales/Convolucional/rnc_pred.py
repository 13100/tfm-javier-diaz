from keras.models import model_from_json
import glob
from keras.preprocessing import image
import numpy as np
import time

from zmq import Context as Context
from zmq import REQ as REQ

context = Context()  # 0.004987-0.002036
socket = context.socket(REQ)
socket.connect("tcp://localhost:5555")
flag_reset = False

json_file = open('model_conv.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("model_conv.h5")
print("Loaded model from disk")
loaded_model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

t = time.time()
cicle = 100
for z in range(cicle):
    if(flag_reset):
        msg = "getImg"
    else:
        msg = "start"
    socket.send_string(msg)
    img = socket.recv_pyobj()
    if(type(img) == type(1)):
        flag_reset = True
    else:
        #test_images = image.load_img(img, target_size=(128, 128))
        test_images = image.img_to_array(img)
        test_images = np.expand_dims(test_images, axis=0)
        #print(img)
        print(loaded_model.predict(test_images))



print(time.time()-t)
print((time.time()-t)/cicle)

"""list = glob.glob(r'dataset\test_image\*.jpg')
pred = np.zeros([len(list)], dtype=object)
for i in range(len(list)):
    test_images = image.load_img(list[i], target_size = (128, 128))
    test_images = image.img_to_array(test_images)
    test_images = np.expand_dims(test_images, axis = 0)
    pred[i] = loaded_model.predict(test_images)
    if(pred[i]==0):
        print(list[i]+": Pred -> Bien")
    else:
        print(list[i] + ": Pred -> Mal")"""

"""path = 'dataset/test_image/pred.bien.X.jpg'

num = 1
for i in range(3):
    str_num = str(num)
    path_in = path.replace("X", str_num)
    if(str_num=="11"):
        path_in = path = 'dataset/test_image/Hadelin_Dog.jpg'
    test_images = image.load_img(path_in, target_size = (128, 128))
    test_images = image.img_to_array(test_images)
    test_images = np.expand_dims(test_images, axis = 0)
    print(loaded_model.predict(test_images)[0][0])
#print(loaded_model.predict(test_images)[0][0])
    if(loaded_model.predict(test_images)[0][0] == 0):
        print(str(num)+": Bien")
    else:
        print(str(num)+": Mal")"""