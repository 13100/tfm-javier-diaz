import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importar el data set
dataset = pd.read_excel('DatabaseANNClean.xlsx')



X = dataset.iloc[:, 1:36].values
x = X
y = dataset.iloc[:, 36].values

"""
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.compose import ColumnTransformer

labelencoder_x1 = LabelEncoder()
X[:,1] = labelencoder_x1.fit_transform(X[:,1]) #Codificar la columna


labelencoder_x2 = LabelEncoder()
X[:,2] = labelencoder_x2.fit_transform(X[:,2])


onehotencoder = OneHotEncoder(X)
ct = ColumnTransformer([('one_hot_encoder', OneHotEncoder(categories='auto'), [1])], remainder='passthrough') #dummy la columna en brakets
X = np.array(ct.fit_transform(X), dtype=np.float)

X=X[:,1:]
"""  #encodificacion de datos categoricos -> no nos interesa, son todo números

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)

import keras
from keras.models import Sequential
from keras.layers import Dense

times = 10
acc = np.zeros([times], dtype=object)
loss = np.zeros([times], dtype=object)

for q in range(times):



#Inicializar la RNA


    classifier = Sequential()

# capa entrada y primera oculta
    un = int(36/2)  #unidades neuronas son la media de las entradas y salidas

    classifier.add(Dense(units=un, kernel_initializer="uniform", activation="relu", input_dim =35))
#nodos cada oculta (experimentar, media capa entrada y salida), inicializador de pesos (uniforme),
#función de activación (relu = rectificador lineal unitario), dimensión datos entrada

#añadir segunda capa oculta
    classifier.add(Dense(units=un, kernel_initializer="uniform", activation="relu"))
    classifier.add(Dense(units=un, kernel_initializer="uniform", activation="relu"))
#la segunda capa oculta ya sabe los datos de entrada, que es el out de la anterior capa


#añadir capa de salida
    classifier.add(Dense(units=1, kernel_initializer="uniform", activation="sigmoid"))
#la función de activación la cambiamos a la sigmoidea

#compilar la rna
    classifier.compile(optimizer ="adam", loss="binary_crossentropy", metrics=["accuracy"])
#optimizador de adam (el default), función de pérdidas (minimización de error de los cuadrados, o la
#diferencia con el logaritmo), métrica de precisión es la regulación automática con precisión,


#ajustamos la rna con el conjunto de entrenamiento
    batch_size = 3
    epochs = 10
    history = classifier.fit(X_train, y_train, batch_size = batch_size, epochs = epochs)
    #entrada, salida, tamaño de bloques a procesar (diferencia entre normal y estocástico),
#número de pasadas

#evaluar el modelo y calcular predicciones finales

    y_pred = classifier.predict(X_test)
    y_pred = (y_pred>0.5)
#filtrar 50% de probabilidad
    print(y_pred)

#elaborar matriz de confusión
    from sklearn.metrics import confusion_matrix
    cm = confusion_matrix(y_test, y_pred)
    accuracy = (cm[0,0]+cm[1,1])/(cm[0,0]+cm[1,1]+cm[1,0]+cm[0,1])
    print("accuracy = %f"%accuracy)
    acc[q] = history.history['accuracy']
    loss[q] = history.history['loss']


    """model_json = classifier.to_json()
    with open("modelANN.json", "w") as json_file:
        json_file.write(model_json)
    classifier.save_weights("modelANN.h5")
    print("Saved model to disk")"""


    plt.plot(history.history['accuracy'])
    plt.plot(history.history['loss'])
    plt.title('model accuracy '+str(batch_size)+"-"+str(epochs))
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'loss'], loc='upper left')
plt.show()
"""
plt.savefig('Test_'+str(batch_size)+"_"+str(epochs)+'.png')
plt.show()
"""