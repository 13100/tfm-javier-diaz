from cv2 import inRange as Separate
from cv2 import putText as WriteText
from cv2 import FONT_HERSHEY_SIMPLEX as FONT_HERSHEY_SIMPLEX
from numpy import zeros as z

from Rellenar import rellenar as rellenar
from Draw import draw as draw


"""def areas_video(start, rango, blurred, gray, radio_max, radio_min, perimeter, rad, deg, deg_360, photo_blank): #version sin zmq
    salto = (255 - start) / rango
    photo_contour = z([rango], dtype=object)
    area = z([rango], dtype=object)

    ##############################################################
    area_blank, photo_full, area_good, area_bad, photo_full_solo, rad, deg, deg_360 = rellenar(blurred, salto,
                                                                                               radio_max, radio_min,
                                                                                               perimeter, rad, deg,
                                                                                               deg_360, rango, start)
    ##############################################################
    WriteText(photo_blank, str(area_blank), (15, 15), FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1)

    for i in range(0, rango - 1):
        area[i] = area_good[i] - area_good[i + 1]
    area[rango - 1] = area_good[rango - 1]
    for i in range(0, rango):
        photo_contour[i] = Separate(blurred, start + salto * i + 1, start + salto * (i + 1))
        WriteText(photo_full[i], str(area_good[i]), (15, 15), FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1)
        WriteText(photo_contour[i], str(area[i]), (15, 15), FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1)
        ##############################################################

        draw(photo_full[i], photo_contour[i], i, rango, photo_full_solo[i])

        ##############################################################
    return [area, area_good, area_blank, rad, deg, deg_360]"""


def areas_zmq(start, rango, blurred, perimeter, rad, deg, deg_360, show):
    salto = (255 - start) / rango
    photo_contour = z([rango], dtype=object)
    area_dif = z([rango], dtype=object)
    list_ellipse = z([rango], dtype=object)
    list_rect = z([rango], dtype=object)
    list_circle = z([rango], dtype=object)
    list_line = z([rango], dtype=object)

    ##############################################################
    """area_blank, photo_full, area_good, photo_full_solo, rad, deg, deg_360, list_ellipse, list_rect, list_circle, list_line = rellenar(
        blurred, salto,
        perimeter, rad, deg,
        deg_360, rango, start)"""

    area_blank, photo_full, area_good, photo_full_solo, rad, deg, deg_360, lists = rellenar(
        blurred, salto,
        perimeter, rad, deg,
        deg_360, rango, start)
    for i in range(rango):
        [list_ellipse[i], list_rect[i], list_circle[i], list_line[i]] = lists[i]

                                                                    #Calcula los datos interesantes
    ##############################################################
    #WriteText(photo_blank, str(area_blank), (15, 15), FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1)

    #print(list_circle, list_line, list_ellipse, list_rect)
    for i in range(0, rango - 1):
        area_dif[i] = area_good[i] - area_good[i + 1]
    area_dif[rango - 1] = area_good[rango - 1]
    for i in range(0, rango):  #Ayuda a la visualización mientras se debugea el código
        photo_contour[i] = Separate(blurred, start + salto * i + 1, start + salto * (i + 1))
        WriteText(photo_full_solo[i], str(area_good[i]), (15, 15), FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1)
        WriteText(photo_contour[i], str(area_dif[i]), (15, 15), FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1)
        ##############################################################
        if(show):

            draw(photo_full[i], photo_contour[i], i, rango, photo_full_solo[i])  #Plotea las separaciones y los datos

        ##############################################################
    return [area_dif, area_good, rad, deg, deg_360, list_ellipse, list_rect, list_circle, list_line]