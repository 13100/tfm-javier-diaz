from Distance import distance as distance

def findCenter(contours):
    aux = [0, 0]
    cont = 0

    for k in range(len(contours)):
        for l in range(len(contours[k])):
            aux[0] += contours[k][l][0][0]
            aux[1] += contours[k][l][0][1]
            cont += 1

    aux[0] = aux[0] / cont
    aux[1] = aux[1] / cont

    far = contours[0][0][0]
    near = contours[0][0][0]
    big = 0
    for t in range(len(contours)):
        if len(contours[t]) > len(contours[big]):
            big = t
    for l in range(len(contours[big])):
        if (distance(aux, contours[big][l][0]) > distance(aux, far)):
            far = contours[big][l][0]
        if (distance(aux, contours[big][l][0]) < distance(aux, near)):
            near = contours[big][l][0]


    dis_max = int(distance(far, aux))
    dis_min = int(distance(near, aux))
    return aux, dis_max, dis_min